﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Resources;
using System.Windows.Shapes;

namespace WPFMapQuestion
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        List<Igrac> igraci = new List<Igrac>();
        Dictionary<string, Button> mapa = new Dictionary<string, Button>();
        public Button Selected { get; set; }
        private static int brojac = 0;

        public MainWindow()
        {
            InitializeComponent();
            Igrac igrac1 = new Igrac("Igrac 1", Brushes.Blue);
            Igrac igrac2 = new Igrac("Igrac 2", Brushes.Green);
            Igrac igrac3 = new Igrac("Igrac 3", Brushes.DeepPink);
            Igrac igrac4 = new Igrac("Igrac 4", Brushes.Red);




            igraci.Add(igrac1);
            igraci.Add(igrac2);
            igraci.Add(igrac3);
            igraci.Add(igrac4);



            //row 1
            mapa.Add("11", button11);
            mapa.Add("21", button12);
            mapa.Add("31", button13);
            mapa.Add("41", button14);
            mapa.Add("51", button15);
            mapa.Add("61", button16);
            mapa.Add("71", button17);
            //row 2
            mapa.Add("12", button21);
            mapa.Add("22", button22);
            mapa.Add("32", button23);
            mapa.Add("42", button24);
            mapa.Add("52", button25);
            mapa.Add("62", button26);
            mapa.Add("72", button27);
            //row 3
            mapa.Add("13", button31);
            mapa.Add("23", button32);
            mapa.Add("33", button33);
            mapa.Add("43", button34);
            mapa.Add("53", button35);
            mapa.Add("63", button36);
            mapa.Add("73", button37);
            //row 4
            mapa.Add("14", button41);
            mapa.Add("24", button42);
            mapa.Add("34", button43);
            mapa.Add("44", button44);
            mapa.Add("54", button45);
            mapa.Add("64", button46);
            mapa.Add("74", button47);
            //row 5
            mapa.Add("15", button51);
            mapa.Add("25", button52);
            mapa.Add("35", button53);
            mapa.Add("45", button54);
            mapa.Add("55", button55);
            mapa.Add("65", button56);
            mapa.Add("75", button57);
            //row 6
            mapa.Add("16", button61);
            mapa.Add("26", button62);
            mapa.Add("36", button63);
            mapa.Add("46", button64);
            mapa.Add("56", button65);
            mapa.Add("66", button66);
            mapa.Add("76", button67);
            //row 7
            mapa.Add("17", button71);
            mapa.Add("27", button72);
            mapa.Add("37", button73);
            mapa.Add("47", button74);
            mapa.Add("57", button75);
            mapa.Add("67", button76);
            mapa.Add("77", button77);

            Igrac1TextBlock.Text = igrac1.ToString();
            Igrac2TextBlock.Text = igrac2.ToString();
            Igrac3TextBlock.Text = igrac3.ToString();
            Igrac4TextBlock.Text = igrac4.ToString();
            naPotezuTextBlock.Text = "Igra: " + naPotezu().ToString();

            PripremiMapu();
            PripremiPocetnoMesto();
            CrtajMogucePokrete();
            //CrtajTeritoriju();
        }

        private Igrac naPotezu()
        {
            return igraci[brojac];
        }

        private void PripremiPocetnoMesto()
        {
            igraci[0].DodajTeritoriju(button11);
            igraci[0].CrtajTeritoriju();
            igraci[1].DodajTeritoriju(button17);
            igraci[1].CrtajTeritoriju();
            igraci[2].DodajTeritoriju(button77);
            igraci[2].CrtajTeritoriju();
            igraci[3].DodajTeritoriju(button71);
            igraci[3].CrtajTeritoriju();
        }

        private void PripremiMapu()
        {
            foreach (KeyValuePair<string, Button> teritory in mapa)
            {
                teritory.Value.Tag = "slobodno";
            }
        }

        private void SledeciNaPotezu()
        {
            if (brojac < 3)
            {
                brojac++;
            }
            else
            {
                brojac = 0;
            }
            OsveziRezultat();
        }

        private void OsveziRezultat()
        {
            Igrac1TextBlock.Text = igraci[0].ToString() + ": " + igraci[0].Teritorija.Count;
            Igrac2TextBlock.Text = igraci[1].ToString() + ": " + igraci[1].Teritorija.Count;
            Igrac3TextBlock.Text = igraci[2].ToString() + ": " + igraci[2].Teritorija.Count;
            Igrac4TextBlock.Text = igraci[3].ToString() + ": " + igraci[3].Teritorija.Count;
            naPotezuTextBlock.Text = naPotezu().ToString();
        }

        private void ButtonClick(object sender, RoutedEventArgs e)
        {
            Button btn = (Button)sender;
            Selected = btn;
            Pitanje ptn = new Pitanje();
            if ((bool)ptn.ShowDialog())
            {
                naPotezu().DodajTeritoriju(btn);
                naPotezu().CrtajTeritoriju();
            }
            OcistiIndikatore();
            SledeciNaPotezu();
            CrtajMogucePokrete();
        }

        private void OcistiIndikatore()
        {
            foreach (KeyValuePair<string, Button> teritory in mapa)
            {

                if (teritory.Value == Selected) // da li je ovo dugme trenutno pritisnuto 
                {
                    teritory.Value.Tag = ""; // ako jeste ignorisi ga za ciscenje jer ce upravo biti osvojeno/izgubljeno
                }

                if ((string)teritory.Value.Tag == "ocistiti")
                {
                    teritory.Value.Background = Brushes.Transparent;
                    teritory.Value.Tag = "slobodno";
                }
            }
        }

        private void CrtajMogucePokrete()
        {
            try
            {
                foreach (Button btn in naPotezu().Teritorija)
                {
                    string s = btn.Name.Replace("button", ""); // button24 => 24
                    int i, j = 0;

                    i = int.Parse(s[0].ToString()); // 24 => 2
                    j = int.Parse(s[1].ToString()); // 24 => 4


                    for (int y = i - 1; y <= i + 1; y++)
                    {
                        if (inRange(y))
                        {
                            for (int x = j - 1; x <= j + 1; x++)
                            {
                                if (inRange(x))
                                {
                                    if (teritorija_Slobodno(mapa[teritorija2ToString(x, y)]))
                                    {
                                        mapa[teritorija2ToString(x, y)].Background = slikaToBackground("slike/pokret_indikator.png");
                                        mapa[teritorija2ToString(x, y)].Tag = "ocistiti";
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Greska!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private bool inRange(int num)
        {
            if (num > 0 && num <= 7)
            {
                return true;
            }
            return false;
        }

        private string teritorija2ToString(int x, int y)
        {
            return string.Format("{0}{1}", x, y);
        }

        private bool teritorija_Slobodno(Button btn)
        {
            if ((string)btn.Tag == "slobodno")
            {
                return true;
            }
            return false;
        }

        private ImageBrush slikaToBackground(string path)
        {
            Uri resourceUri = new Uri("slike/pokret_indikator.png", UriKind.Relative);
            StreamResourceInfo streamInfo = Application.GetResourceStream(resourceUri);

            BitmapFrame temp = BitmapFrame.Create(streamInfo.Stream);
            var slika = new ImageBrush();
            slika.ImageSource = temp;

            return slika;
        }

        private void OAutoru_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Napravljeno 20/05/2017 \nProgramiranje: Luka Žagar\nIzrada baze: Mirko Vojinović", "Autor", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        private void podesiIgrace_Click(object sender, RoutedEventArgs e)
        {
            podesiIgrace podesi = new podesiIgrace();
            podesi.ShowDialog();
        }

        private void unesiPitanja_Click(object sender, RoutedEventArgs e)
        {
            unesiPitanje unesi = new unesiPitanje();
            unesi.ShowDialog();
        }
    }
}
