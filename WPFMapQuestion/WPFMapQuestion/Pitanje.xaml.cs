﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WPFMapQuestion
{
    /// <summary>
    /// Interaction logic for Pitanje.xaml
    /// </summary>
    public partial class Pitanje : Window
    {
        public List<Button> options = new List<Button>();
        public Pitanje()
        {
            InitializeComponent();
            options.Add(opcija1);
            options.Add(opcija2);
            options.Add(opcija3);
            options.Add(opcija4);
            UcitajPodatke();
        }

        private void UcitajPodatke()
        {
            try
            {
                string querry = "SELECT Pitanje.Pitanje, Odgovor.Odgovor, Odgovor.Tacan " +
                "FROM Pitanje,Grupa_Odgovora,Odgovor " +
                "WHERE Pitanje.PitanjeID = Grupa_Odgovora.PitanjeID " +
                "AND Grupa_Odgovora.GrupaOdgovoraID = Odgovor.GrupaOdgovoraID ";
                DataTable pitanje = Database.ExecuteQuery(querry);
                pitanjeTextBlock.Text = pitanje.Rows[0][0].ToString();
                for (int i = 0; i < pitanje.Rows.Count; i++)
                {
                    options[i].Content = pitanje.Rows[i][1].ToString();
                    options[i].Tag = pitanje.Rows[i][2].ToString();
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void opcija_Click(object sender, RoutedEventArgs e)
        {
            Button btn = (Button)sender;
            if(btn.Tag.ToString() == "True")
            {
                MessageBox.Show("Tačno!");
                DialogResult = true;
                return;
            }
            MessageBox.Show("Netačno");
            DialogResult = false;
            Close();
        }
        
    }
    
}
