﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media;

namespace WPFMapQuestion
{
    class Igrac
    {
        public string Ime { get; set; }
        public List<Button> Teritorija { get; set; }
        public Brush Boja_Teritorije { get; set; }

        public Igrac(string _ime , Brush _bojaTeritorije)
        {
            this.Ime = _ime;
            this.Boja_Teritorije = _bojaTeritorije;
            this.Teritorija = new List<Button>();
        }

        public void DodajTeritoriju(Button _teritorija)
        {
            Teritorija.Add(_teritorija);
            _teritorija.Tag = "osvojeno";
        }

        public void CrtajTeritoriju()
        {
            foreach (Button btn in Teritorija)
            {
                btn.Background = Boja_Teritorije;
            }
        }

        public override string ToString()
        {
            return Ime;
        }
    }
}
