﻿

CREATE TABLE Pitanje
(
	PitanjeID INT IDENTITY PRIMARY KEY NOT NULL,
	Pitanje NVARCHAR(MAX)
);

CREATE TABLE Grupa_Odgovora
(
	GrupaOdgovoraID INT IDENTITY PRIMARY KEY NOT NULL,
	PitanjeID INT FOREIGN KEY REFERENCES Pitanje(PitanjeID) NOT NULL,
);

CREATE TABLE Odgovor
(
	OdgovorID INT PRIMARY KEY IDENTITY NOT NULL,
	GrupaOdgovoraID INT FOREIGN KEY REFERENCES Grupa_Odgovora(GrupaOdgovoraID) NOT NULL,
	Odgovor NVARCHAR(MAX) NOT NULL,
	Tacan BIT NOT NULL
);

INSERT INTO Pitanje (Pitanje) VALUES ('Koji broj dolazi sledeći? 1-1-2-3-5-8-13');
INSERT INTO Grupa_Odgovora(PitanjeID) VALUES (1);
INSERT INTO Odgovor(GrupaOdgovoraID,Odgovor,Tacan) VALUES (1,'8',0);
INSERT INTO Odgovor(GrupaOdgovoraID,Odgovor,Tacan) VALUES (1,'13',0);
INSERT INTO Odgovor(GrupaOdgovoraID,Odgovor,Tacan) VALUES (1,'21',1);
INSERT INTO Odgovor(GrupaOdgovoraID,Odgovor,Tacan) VALUES (1,'26',0);


