﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WPFMapQuestion
{
    /// <summary>
    /// Interaction logic for unesiPitanje.xaml
    /// </summary>
    public partial class unesiPitanje : Window
    {
        public unesiPitanje()
        {
            InitializeComponent();
            MessageBox.Show("FORMA TRENUTNO NERADI");
        }

        private void UnesiPitanje_Click(object sender, RoutedEventArgs e)
        {
            if (UneteVrednosti())
            {
                try
                {
                    string pitanje = "INSERT INTO Pitanje(Pitanje) VALUES('" + pitanjeTextBlock.Text + "');";
                    DataTable dt = Database.ExecuteQuery("SELECT COUNT(PitanjeID) FROM Pitanje"); // EXECUTE NON QUERRY NERADI , VRACA -1
                    int count = int.Parse(dt.Rows[0][0].ToString());
                    count++;// count +1 jer unosimo sledecu vrednost
                    string GrupaOdgovora = "INSERT INTO Grupa_Odgovora(PitanjeID) VALUES('" + count.ToString() + "');";
                    string Odgovor1 = string.Format("INSERT INTO Odgovor(GrupaOdgovoraID, Odgovor, Tacan) VALUES({0},'{1}',{2});",count.ToString(), opcija1TextBox.Text.ToString(), tacnaVrednost(opcija1).ToString());
                    string Odgovor2 = string.Format("INSERT INTO Odgovor(GrupaOdgovoraID, Odgovor, Tacan) VALUES({0},'{1}',{2});", count.ToString(), opcija2TextBox.Text.ToString(), tacnaVrednost(opcija2).ToString());
                    string Odgovor3 = string.Format("INSERT INTO Odgovor(GrupaOdgovoraID, Odgovor, Tacan) VALUES({0},'{1}',{2});", count.ToString(), opcija3TextBox.Text.ToString(), tacnaVrednost(opcija3).ToString());
                    string Odgovor4 = string.Format("INSERT INTO Odgovor(GrupaOdgovoraID, Odgovor, Tacan) VALUES({0},'{1}',{2});", count.ToString(), opcija4TextBox.Text.ToString(), tacnaVrednost(opcija4).ToString());


                    Database.ExecuteNonQuery(pitanje);
                    Database.ExecuteNonQuery(GrupaOdgovora);
                    Database.ExecuteNonQuery(Odgovor1);
                    Database.ExecuteNonQuery(Odgovor2);
                    Database.ExecuteNonQuery(Odgovor3);
                    Database.ExecuteNonQuery(Odgovor4);

                    MessageBox.Show("Uspesno uneto u bazu!", "Uspesno obavljeno", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "GRESKA", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            else
            {
                MessageBox.Show("Niste uneli vrednosti za sva polja ili koristite glagoljicu!", "Greska!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            
        }
       
        private bool UneteVrednosti()
        {
            if(opcija1TextBox.Text != "" && opcija2TextBox.Text != "" && opcija3TextBox.Text != "" && opcija4TextBox.Text != "" && pitanjeTextBlock.Text != "")
            {
                return true;
            }
            return false;
        }

        private int tacnaVrednost(Button btn)
        {
            return ((btn.Tag.ToString() == "true") ? 1 : 0);
        }
        
        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            opcija1.Tag = "true";
        }

        private void CheckBox_Checked_1(object sender, RoutedEventArgs e)
        {
            opcija2.Tag = "true";
        }

        private void CheckBox_Checked_2(object sender, RoutedEventArgs e)
        {
            opcija3.Tag = "true";
        }

        private void CheckBox_Checked_3(object sender, RoutedEventArgs e)
        {
            opcija4.Tag = "true";
        }
    }
}
